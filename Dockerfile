FROM gitpod/workspace-full

USER root

RUN apt-get update && apt-get install -y git fakeroot build-essential ncurses-dev xz-utils libssl-dev bc flex ninja-build libelf-dev bison && apt-get clean && rm -rf /var/cache/apt/* && rm -rf /var/lib/apt/lists/* && rm -rf /tmp/*